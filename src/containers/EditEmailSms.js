import React from 'react';
import EditEmail from '../components/EditEmailSms/EditEmail';
import EditSms from '../components/EditEmailSms/EditSms';


const EditEmailSms = () => {
    return (
        <div className="row">
            <div className="col-xs-12 col-sm-12 col-md-6 col-lg-6 m-b-15 ">
                <EditEmail />
            </div>
            <div className="col-xs-12 col-sm-12 col-md-6 col-lg-6 m-b-15 ">
                <EditSms />
            </div>
        </div>
    );
};


export default EditEmailSms;