import React, { PropTypes, Component } from 'react';
import { Link } from 'react-router';
import { Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn } from 'material-ui/Table';
import FloatingActionButton from 'material-ui/FloatingActionButton';
import ContentCreate from 'material-ui/svg-icons/content/create';
import ContentAdd from 'material-ui/svg-icons/content/add';
import ContentCancel from 'material-ui/svg-icons/content/clear';
import { pink500, grey200, grey500 } from 'material-ui/styles/colors';
import PageBase from '../components/PageBase';
import CircularProgress from 'material-ui/CircularProgress';
import api from '../api/api';

class TablePage extends Component {

  constructor(props) {
    super(props);
    this.state = {
      data: [],
      loading: false
    };
  }

  componentDidMount(){
    this.setState( {loading: true} );
    api.data.getCancelledAppointments().then(cancelledAppoinments =>{
      let cAppointments = cancelledAppoinments;
      this.setState({ data: cAppointments, loading: false });
    });
  }

  render() {
    if (this.state.loading) {
      return (<div style={{ marginLeft: '45%', marginTop: '25%' }}>
        <CircularProgress size={80} thickness={5} />
      </div>
      );
    } else {
      return (
        <PageBase title="Cancelled Appointments"
          navigation="Application / Cancelled Apointments" >
          <div style={{ overflow: 'auto', height: 400 }}>
            <Link to="/form" >
              <FloatingActionButton style={styles.floatingActionButton} backgroundColor={pink500}>
                <ContentAdd />
              </FloatingActionButton>
            </Link>

            <Table>
              <TableHeader displaySelectAll={false} adjustForCheckbox={false}>
                <TableRow>
                  <TableHeaderColumn style={styles.columns.id}>ID</TableHeaderColumn>
                  <TableHeaderColumn style={styles.columns.name}>Name</TableHeaderColumn>
                  <TableHeaderColumn style={styles.columns.phone}>Phone</TableHeaderColumn>
                  <TableHeaderColumn style={styles.columns.date}>Date</TableHeaderColumn>
                  <TableHeaderColumn style={styles.columns.time}>Time</TableHeaderColumn>
                  <TableHeaderColumn style={styles.columns.edit}>Edit</TableHeaderColumn>
                  <TableHeaderColumn style={styles.columns.cancel}>Cancel</TableHeaderColumn>
                </TableRow>
              </TableHeader>
              <TableBody displayRowCheckbox={false} stripedRows={true}>
                {this.state.data.map(item =>
                  <TableRow key={item.id}>
                    <TableRowColumn style={styles.columns.id}>{item.id}</TableRowColumn>
                    <TableRowColumn style={styles.columns.name}>{item.patientName}</TableRowColumn>
                    <TableRowColumn style={styles.columns.phone}>{item.contactNo}</TableRowColumn>
                    <TableRowColumn style={styles.columns.date}>{item.date}</TableRowColumn>
                    <TableRowColumn style={styles.columns.time}>{item.time}</TableRowColumn>
                    <TableRowColumn style={styles.columns.edit}>
                      <Link className="button" to="/form">
                        <FloatingActionButton zDepth={0}
                          mini={true}
                          backgroundColor={grey200}
                          iconStyle={styles.editButton}>
                          <ContentCreate />
                        </FloatingActionButton>
                      </Link>
                    </TableRowColumn>
                    <TableRowColumn style={styles.columns.edit}>
                      <Link className="button" to="/form">
                        <FloatingActionButton zDepth={0}
                          mini={true}
                          backgroundColor={grey200}
                          iconStyle={styles.editButton}>
                          <ContentCancel />
                        </FloatingActionButton>
                      </Link>
                    </TableRowColumn>
                  </TableRow>
                )}
              </TableBody>
            </Table>
          </div>
        </PageBase>
      );
    }
  }
}

const styles = {
  floatingActionButton: {
    margin: 0,
    top: 'auto',
    right: 20,
    bottom: 20,
    left: 'auto',
    position: 'fixed',
  },
  editButton: {
    fill: grey500
  },
  columns: {
    id: {
      width: '5%'
    },
    name: {
      width: '25%'
    },
    phone: {
      width: '20%'
    },
    date: {
      width: '15%'
    },
    time: {
      width: '25%'
    },
    edit: {
      width: '10%'
    },
    cancel: {
      width: '10%'
    }
  }
};


TablePage.propTypes = {
  data: PropTypes.array
};

export default TablePage;
