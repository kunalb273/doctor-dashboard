import React, { PropTypes, Component } from 'react';
import { cyan600, pink600, purple600, orange600 } from 'material-ui/styles/colors';
import TodaysAppointmentIcon from 'material-ui/svg-icons/action/assignment';
import PendingAppointmentIcon from 'material-ui/svg-icons/action/assignment-late';
import SuccessfullAppointmentIcon from 'material-ui/svg-icons/action/assignment-turned-in';
import CancelledAppointmentIcon from 'material-ui/svg-icons/content/clear';
import InfoBox from '../components/dashboard/InfoBox';
import BrowserUsage from '../components/dashboard/BrowserUsage';
import RecentlyProducts from '../components/dashboard/RecentlyProducts';
import globalStyles from '../styles';
import Data from '../data';
import CircularProgress from 'material-ui/CircularProgress';
import api from '../api/api';

class DashboardPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      todayAppointmets: '',
      pendingAppointments: '',
      cancelledAppointments: '',
      successfulAppointments: '',
      loading: false
    };
  }
  componentDidMount() {
    this.setState({ loading: true });
    api.data.getDashBoardData().then(loggedInDoctorDetails => {
      let tAppointmet = loggedInDoctorDetails[0].appointmentCardDetails.todayAppointment.toString();
      let pAppointment = loggedInDoctorDetails[0].appointmentCardDetails.pendingAppointment.toString();
      let cAppointments = loggedInDoctorDetails[0].appointmentCardDetails.cancelledAppointment.toString();
      let sAppointments = loggedInDoctorDetails[0].appointmentCardDetails.successfulAppointment.toString();
      this.setState({ todayAppointmets: tAppointmet, pendingAppointments: pAppointment, cancelledAppointments: cAppointments, successfulAppointments: sAppointments, loading: false });
    });
  }

  render() {
    if (this.state.loading) {
      return (<div style={{ marginLeft: '45%', marginTop: '25%' }}>
        <CircularProgress size={80} thickness={5} />
      </div>
      );
    } else {
      return (
        <div>
          <h3 style={globalStyles.navigation}>Application / Dashboard</h3>

          <div className="row">

            <div className="col-xs-12 col-sm-6 col-md-3 col-lg-3 m-b-15 ">
              <InfoBox Icon={TodaysAppointmentIcon}
                color={pink600}
                title="Todays Appointment"
                value={this.state.todayAppointmets}
              />
            </div>


            <div className="col-xs-12 col-sm-6 col-md-3 col-lg-3 m-b-15 ">
              <InfoBox Icon={PendingAppointmentIcon}
                color={cyan600}
                title="Pending Appointments"
                value={this.state.pendingAppointments}
              />
            </div>

            <div className="col-xs-12 col-sm-6 col-md-3 col-lg-3 m-b-15 ">
              <InfoBox Icon={CancelledAppointmentIcon}
                color={purple600}
                title="Cancelled Appintments"
                value={this.state.cancelledAppointments}
              />
            </div>

            <div className="col-xs-12 col-sm-6 col-md-3 col-lg-3 m-b-15 ">
              <InfoBox Icon={SuccessfullAppointmentIcon}
                color={orange600}
                title="Successful Appoinments"
                value={this.state.successfulAppointments}
              />
            </div>
          </div>

          <div className="row">
            <div className="col-xs-12 col-sm-12 col-md-6 col-lg-6 m-b-15 ">
              <RecentlyProducts data={Data.dashBoardPage.recentProducts} />
            </div>
            <div className="col-xs-12 col-sm-12 col-md-6 col-lg-6 m-b-15 ">
              <BrowserUsage data={Data.dashBoardPage.browserUsage} />
            </div>
          </div>
        </div>
      );
    }
  }
}

DashboardPage.propTypes = {
  data: PropTypes.array
};


export default DashboardPage;
