import React, { PropTypes } from 'react';
import Drawer from 'material-ui/Drawer';
import { spacing, typography } from 'material-ui/styles';
import { white, blue600, black } from 'material-ui/styles/colors';
import MenuItem from 'material-ui/MenuItem';
import { Link } from 'react-router';
import Avatar from 'material-ui/Avatar';
import Assessment from 'material-ui/svg-icons/action/assessment';
import ArrowDropRight from 'material-ui/svg-icons/navigation-arrow-drop-right';
import Email from 'material-ui/svg-icons/editor/border-color';
import GridOn from 'material-ui/svg-icons/image/grid-on';
import Web from 'material-ui/svg-icons/av/web';

const LeftDrawer = (props) => {
  let { navDrawerOpen } = props;

  const styles = {
    logo: {
      cursor: 'pointer',
      fontSize: 22,
      color: typography.textFullWhite,
      lineHeight: `${spacing.desktopKeylineIncrement}px`,
      fontWeight: typography.fontWeightLight,
      backgroundColor: blue600,
      paddingLeft: 40,
      height: 56,
    },
    submenuitems:{
      color: black,
      fontSize: 18
    },
    menuItem: {
      color: white,
      fontSize: 18
    },
    avatar: {
      div: {
        padding: '15px 0 20px 15px',
        backgroundImage: 'url(' + require('../images/material_bg.png') + ')',
        height: 45
      },
      icon: {
        float: 'left',
        display: 'block',
        marginRight: 15,
        boxShadow: '0px 0px 0px 8px rgba(0,0,0,0.2)'
      },
      span: {
        paddingTop: 12,
        display: 'block',
        color: 'white',
        fontWeight: 300,
        textShadow: '1px 1px #444'
      }
    }
  };

  return (
    <Drawer
      docked={true}
      open={navDrawerOpen}>
      <div style={styles.logo}>
        Material Admin
        </div>
      <div style={styles.avatar.div}>
        <Avatar src="http://www.material-ui.com/images/uxceo-128.jpg"
          size={50}
          style={styles.avatar.icon} />
        <span style={styles.avatar.span}>{props.username}</span>
      </div>
      <div>
          <MenuItem
            style={styles.menuItem}
            primaryText="Home"
            leftIcon={<Assessment/>}
            containerElement={<Link to="/dashboard" />}
          />
        <MenuItem
          primaryText="Appointment's"
          leftIcon={<GridOn/>}
          style={{color:"#fff"}}
          rightIcon={<ArrowDropRight />}
          menuItems={[
            <MenuItem key="one" primaryText="All Appointments" style={styles.submenuItem} containerElement={<Link to="/table" />}/>,
            <MenuItem key="two" primaryText="Pending Appointments" style={styles.submenuItem} containerElement={<Link to="/pending" />}/>,
            <MenuItem key="three" primaryText="Cancelled Appointments" style={styles.submenuItem} containerElement={<Link to="/cancelled" />}/>,
          ]}
        />
        <MenuItem
            style={styles.menuItem}
            primaryText="Book Appoinment"
            leftIcon={<Web/>}
            containerElement={<Link to="/form" />}
          />
        <MenuItem
            style={styles.menuItem}
            primaryText="Edit email/sms"
            leftIcon={<Email/>}
            containerElement={<Link to="/editemailsms" />}
          />
      </div>
    </Drawer>
  );
};

LeftDrawer.propTypes = {
  navDrawerOpen: PropTypes.bool,
  menus: PropTypes.array,
  username: PropTypes.string,
};

export default LeftDrawer;
