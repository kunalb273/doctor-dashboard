import React, {PropTypes} from 'react';
import {Link} from 'react-router';
import AppBar from 'material-ui/AppBar';
import IconButton from 'material-ui/IconButton';
import IconMenu from 'material-ui/IconMenu';
import MenuItem from 'material-ui/MenuItem';
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert';
import Menu from 'material-ui/svg-icons/navigation/menu';
import {white, red700} from 'material-ui/styles/colors';
import PowerIcon from 'material-ui/svg-icons/action/power-settings-new';
import { logout } from "../actions/auth";
import { connect } from "react-redux";
import {browserHistory} from 'react-router';


class Header extends React.Component {
  handleClick = () => {
    alert('hello');
  }
  render() {
    const {styles, handleChangeRequestNavDrawer} = this.props;
    const style = {
      appBar: {
        position: 'fixed',
        top: 0,
        overflow: 'hidden',
        maxHeight: 57
      },
      menuButton: {
        marginLeft: 10
      },
      iconsRightContainer: {
        marginLeft: 20
      }
    };

    // if (token==null) {
    //   <Link to="/login" />
    // }

    return (
        <div>
            <AppBar
              style={{...styles, ...style.appBar}}
              
              iconElementLeft={
                  <IconButton style={style.menuButton} onClick={handleChangeRequestNavDrawer}>
                    <Menu color={white} />
                  </IconButton>
              }
              iconElementRight={
                <div style={style.iconsRightContainer}>
                  
                  <PowerIcon hoverColor={red700} color={white} style={{marginRight:20, marginTop:10 } } onClick={() => this.props.logout()} />
                  </div>
              }
            />
          </div>
      );
  }
}

Header.propTypes = {
  styles: PropTypes.object,
  handleChangeRequestNavDrawer: PropTypes.func,
  logout: PropTypes.func.isRequired
};

export default connect(null, { logout })(Header);
