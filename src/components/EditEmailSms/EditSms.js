import React from 'react';
import TextField from 'material-ui/TextField/TextField';
import Button from 'material-ui/RaisedButton/RaisedButton';
import { Card, CardHeader, CardMedia, CardTitle, CardText } from 'material-ui/Card';

const EditSms = () => {


    const style = {
        margin: 12,
    };
    return (
        <Card style={{ padding: 5 }}>
            <CardHeader />
            <br /><br /><br />
            <CardMedia
                overlay={<CardTitle title="SMS" subtitle="edit your message body here...." />}
            />
            <CardTitle subtitle="use <NAME>, <DATE>, <TIME> instead of real values" />
            <CardText>
                <TextField defaultValue="Dear <NAME> your appointment with <DOCTOR> has been confirmed on <DATE> at <TIME>at Lilavati Hospital Mumbai, Maharastra, India , World Earth, Milkeyway Galaxy. For more details contact Hospital Care at : 18001809999" style={{ width: '100%' }}
                hintText="Message Field"
                    floatingLabelText="Write Your E-Mail here......."
                    multiLine={true}
                rows={8}
                />
            </CardText>
            <Button label="UPDATE" primary={true} style={style} />
        </Card>
    );
};

export default EditSms;
