import React, { PropTypes, Component } from 'react';
import Paper from 'material-ui/Paper';
import { PieChart, Pie, Cell, ResponsiveContainer } from 'recharts';
import Avatar from 'material-ui/Avatar';
import List from 'material-ui/List/List';
import ListItem from 'material-ui/List/ListItem';
import GlobalStyles from '../../styles';
import { cyan600, pink600, white } from 'material-ui/styles/colors';
import Subheader from 'material-ui/Subheader';
import { typography } from 'material-ui/styles';
import api from '../../api/api';

class BrowserUsage extends Component {
  constructor(props) {
    super(props);

    this.state = {
      used: '',
      left: ''
    };
  }


  componentDidMount() {
    api.data.getSmsData().then(loggedInDoctorDetails =>{
      let uSMS =loggedInDoctorDetails[0].smsDetails.used;
      let lSMS =loggedInDoctorDetails[0].smsDetails.left;
      this.setState({ used: uSMS, left: lSMS});
    }); 
  }


  render() {
      return (
        <Paper style={styles.paper}>
          <div>
            <Subheader style={styles.subheader}>Sms Details</Subheader>
          </div>

          <div style={GlobalStyles.clear} />
          <div className="row">
            <div className="col-xs-12 col-sm-8 col-md-8 col-lg-8">
              <div style={styles.pieChartDiv}>
                <ResponsiveContainer>
                  <PieChart>
                    <Pie
                      innerRadius={80}
                      outerRadius={130}
                      fill="#8884d8">
                      <Cell value={this.state.used} fill={pink600} />
                      <Cell value={this.state.left} fill={cyan600} />
                    </Pie>
                  </PieChart>
                </ResponsiveContainer>
              </div>
            </div>

            <div className="col-xs-12 col-sm-4 col-md-4 col-lg-4">
              <div style={styles.legend}>
                <div style={styles.legend}>
                  <List>
                    <ListItem
                      leftAvatar={
                        <Avatar children={this.state.used}
                          style={{ padding: 5 }}
                          backgroundColor={pink600} />
                      }>
                      Used
                      </ListItem>
                    <ListItem
                      leftAvatar={
                        <Avatar children={this.state.left}
                          style={{ padding: 5 }}
                          backgroundColor={cyan600} />
                      }>
                      Left
                      </ListItem>
                  </List>
                </div>
              </div>
            </div>
          </div>
        </Paper>
      );
    }
  }

const styles = {
  paper: {
    minHeight: 344,
  },
  legend: {
    paddingTop: 20,
  },
  pieChartDiv: {
    height: 290,
    textAlign: 'center'
  },
  subheader: {
    fontSize: 24,
    fontWeight: typography.fontWeightLight,
    backgroundColor: cyan600,
    color: white
  }
};

BrowserUsage.propTypes = {
  data: PropTypes.array
};

export default BrowserUsage;
