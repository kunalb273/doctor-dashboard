import React, { PropTypes, Component } from 'react'
import Avatar from 'material-ui/Avatar';
import { List, ListItem } from 'material-ui/List';
import Subheader from 'material-ui/Subheader';
import Divider from 'material-ui/Divider';
import Paper from 'material-ui/Paper';
import IconButton from 'material-ui/IconButton';
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert';
import IconMenu from 'material-ui/IconMenu';
import MenuItem from 'material-ui/MenuItem';
import { grey400, cyan600, white } from 'material-ui/styles/colors';
import { typography } from 'material-ui/styles';
import Wallpaper from 'material-ui/svg-icons/device/wallpaper';
import axios from 'axios';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import RaisedButton from 'material-ui/RaisedButton';
import { Card, CardHeader, CardMedia, CardTitle, CardText } from 'material-ui/Card';
import api from '../../api/api';


class RecentlyProducts extends Component {
  constructor(props) {
    super(props);

    this.state = {
      open: false,
      data: []
    };
  }


  componentDidMount() {
    api.data.getTodaysAppointment().then(todaysAppointments => {
    var todaysAppointments = todaysAppointments;
    this.setState({ data: todaysAppointments });
    });
  }

  handleOpen = () => {
    this.setState({ open: true });
  };

  handleClose = () => {
    this.setState({ open: false });
  };

  render() {
    return (
      <Paper style={{ height: '480', overflow: 'auto' }}>
        <List>
          <Subheader style={styles.subheader}>Today's Appointments</Subheader>
          {this.state.data.map(item =>
            <div key={item.patientName}>
              <ListItem
                leftAvatar={<Avatar icon={<Wallpaper />} />}
                primaryText={item.patientName}
                secondaryText={item.time}
                rightIconButton={
                  <div>
                    <IconButton><MoreVertIcon color={'#28ACC1'} onClick={this.handleOpen} /></IconButton>
                    <Dialog  
                            modal={false} open={this.state.open} 
                            onRequestClose={this.handleClose}
                            bodyStyle={{ backgroundColor: '#fff' }}>

                      <CardHeader /><br />
                      <CardMedia style={{color:'#fff', fontSize:30}} overlay={<CardTitle style={{backgroundColor:'#4caf50'}}>Patient's Detail</CardTitle>}>
                      </CardMedia>
                      <div style={{width:'40%',float:'left'}}>
                      <Card style={{ height:200, padding:10, margin:10,marginRight:0, backgroundColor:'#ffc107'}}>
                          <CardText style={{textAlign: "center", marginTop:'20%', color:'#fff',fontSize:40, fontWeight:'bold'}}>9:50 AM</CardText>
                          <CardText style={{fontSize:20, textAlign:'center', color:'#fff', fontWeight:'bold'}}>8th November 2017</CardText>
                      </Card>
                      </div>
                      <div style={{marginLeft:'44%'}}>
                      <Card style={{ height:200, margin:10,marginLeft:-10, backgroundColor:'#42a5f5'}}> 
                        <CardText style={{fontSize:20, color:'#fff'}} >
                      <b>Name:</b> Kunal Chorasiya<br/>
                      <b>Phone:</b> 9211420420<br/>
                      <b>Age:</b> 88<br/>
                      <b>Gender:</b> Male<br/>
                      <b>Address:</b> la pata<br/>
                      </CardText>
                      </Card>
                      </div>
                    </Dialog>
                  </div>
                }
              />
              <Divider inset={true} />
            </div>
          )}
        </List>
      </Paper>
    )
  }
}

const styles = {
  subheader: {
    fontSize: 24,
    fontWeight: typography.fontWeightLight,
    backgroundColor: cyan600,
    color: white
  }
};

const iconButtonElement = (
  <IconButton
    touch={true}
    tooltipPosition="bottom-left"
  >
    <MoreVertIcon color={grey400} />
  </IconButton>
);

// const rightIconMenu = (
//   <IconMenu iconButtonElement={iconButtonElement}>
//     <MenuItem primaryText="Details" ></MenuItem>
//   </IconMenu>
// );

RecentlyProducts.propTypes = {
  data: PropTypes.array
};

export default RecentlyProducts;
