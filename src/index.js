/* eslint-disable import/default */

import React from 'react';
import { render } from 'react-dom';
import { Router, browserHistory } from 'react-router';
import routes from './routes';
import injectTapEventPlugin from 'react-tap-event-plugin';
require('./favicon.ico');
import './styles.scss';
import 'font-awesome/css/font-awesome.css';
import 'flexboxgrid/css/flexboxgrid.css';
import rootReducer from '../src/reducers/rootReducer';
import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import { userLoggedIn } from './actions/auth';
import { composeWithDevTools } from 'redux-devtools-extension';
import thunk from 'redux-thunk';


const store = createStore(
    rootReducer,
    composeWithDevTools(applyMiddleware(thunk))
);

if (localStorage.userJWT) {
    const user = localStorage.userJWT;
    store.dispatch(userLoggedIn(user));
}

injectTapEventPlugin();


render(
    <Provider store={store}>
        <Router routes={routes} history={browserHistory} />
    </Provider>
    ,document.getElementById('app')
);
