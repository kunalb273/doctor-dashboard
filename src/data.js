import React from 'react';
import Assessment from 'material-ui/svg-icons/action/assessment';
import Email from 'material-ui/svg-icons/editor/border-color';
import GridOn from 'material-ui/svg-icons/image/grid-on';
import Web from 'material-ui/svg-icons/av/web';
import {cyan600, pink600} from 'material-ui/styles/colors';

const data = {
  menus: [
    { text: 'Home', icon: <Assessment/>, link: '/dashboard' },
    { text: "All Appointment's", icon: <GridOn/>, link: '/table' },
    { text: "Book Apppintment's", icon: <Web/>, link: '/form' },
    { text: "Edit Email/SMS", icon: <Email/>, link: '/editemailsms' },
  ],
  tablePage: {
    items: [
      {id: 1, name: 'Product 1', phone: '$50.00', date: '12-11-2017 ', time: '8:00 PM'},
      {id: 2, name: 'Product 2', phone: '$150.00', date: '12-11-2017 ', time: '8:00 PM'},
      {id: 3, name: 'Product 3', phone: '$250.00', date: '12-11-2017 ', time: '8:00 PM'},
      {id: 4, name: 'Product 4', phone: '$70.00', date: '12-11-2017 ', time: '8:00 PM'},
      {id: 5, name: 'Product 5', phone: '$450.00', date: '12-11-2017 ', time: '8:00 PM'},
      {id: 6, name: 'Product 6', phone: '$950.00', date: '12-11-2017 ', time: '8:00 PM'},
      {id: 7, name: 'Product 7', phone: '$550.00', date: '12-11-2017 ', time: '8:00 PM'},
      {id: 8, name: 'Product 8', phone: '$750.00', date: '12-11-2017 ', time: '8:00 PM'}
    ]
  },
  dashBoardPage: {
    recentProducts: [
      {id: 1, title: 'Sanjay', text: '8:00 AM'},
      {id: 2, title: 'Kunal', text: '9:00 AM'},
      {id: 3, title: 'Atul', text: '11:00 Am'},
      {id: 4, title: 'Aakash', text: '04:00 PM'}
    ],
    monthlySales: [
      {name: 'Jan', uv: 3700},
      {name: 'Feb', uv: 3000},
      {name: 'Mar', uv: 2000},
      {name: 'Apr', uv: 2780},
      {name: 'May', uv: 2000},
      {name: 'Jun', uv: 1800},
      {name: 'Jul', uv: 2600},
      {name: 'Aug', uv: 2900},
      {name: 'Sep', uv: 3500},
      {name: 'Oct', uv: 3000},
      {name: 'Nov', uv: 2400},
      {name: 'Dec', uv: 2780}
    ],
    newOrders: [
      {pv: 2400},
      {pv: 1398},
      {pv: 9800},
      {pv: 3908},
      {pv: 4800},
      {pv: 3490},
      {pv: 4300}
    ],
    browserUsage: [
      {name: 'Used', value: 100, color: cyan600, usedSms: 100},
      {name: 'Left', value: 300, color: pink600, usedSms: 300},
    ]
  }
};

export default data;
