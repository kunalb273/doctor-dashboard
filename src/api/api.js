import axios from 'axios';

export default {
    data: {
        getDashBoardData: dashBoardData =>
        axios.get("http://localhost:3000/db").then(res => res.data.loggedInDoctorDetails),
        getSmsData: smsData=>
        axios.get("http://localhost:3000/db").then(res => res.data.loggedInDoctorDetails),
        getTodaysAppointment: todaysAppointment=>
        axios.get("http://localhost:3000/db").then(res => res.data.todaysAppointments),
        getAllAppointments: allAppointments=>
        axios.get("http://localhost:3000/db").then(res => res.data.allAppointments),
        getPendingAppointments: pendingAppointments=>
        axios.get("http://localhost:3000/db").then(res => res.data.pendingAppointments),
        getCancelledAppointments: cancelledAppointments=>
        axios.get("http://localhost:3000/db").then(res => res.data.cancelledAppointments),
    },
    user:{
        login: credentials => 
        axios.post("http://192.168.1.5:8000/api/auth", {credentials}).then(res => res.data.user)
    }
};